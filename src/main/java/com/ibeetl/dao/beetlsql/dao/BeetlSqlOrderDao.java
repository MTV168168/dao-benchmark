package com.ibeetl.dao.beetlsql.dao;



import com.ibeetl.dao.beetlsql.entity.BeetlSqlOrder;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.SqlResource;

@SqlResource("order")
public interface BeetlSqlOrderDao extends BaseMapper<BeetlSqlOrder> {
	
}