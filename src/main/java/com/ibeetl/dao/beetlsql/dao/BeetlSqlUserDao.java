package com.ibeetl.dao.beetlsql.dao;


import org.beetl.sql.core.engine.PageQuery;

import com.ibeetl.dao.beetlsql.entity.BeetlSqlUser;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.Param;
import org.beetl.sql.mapper.annotation.SqlResource;

@SqlResource("user")
public interface BeetlSqlUserDao extends BaseMapper<BeetlSqlUser> {
	public void selectUser(PageQuery page);
	public BeetlSqlUser findUserAndDepartment(@Param("id") Integer id);
}