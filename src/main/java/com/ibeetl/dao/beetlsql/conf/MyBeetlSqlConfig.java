package com.ibeetl.dao.beetlsql.conf;

import org.beetl.sql.core.Interceptor;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.UnderlinedNameConversion;
import org.beetl.sql.core.db.H2Style;
import org.beetl.sql.starter.SQLManagerCustomize;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



@Configuration
public class MyBeetlSqlConfig {

	@Bean
	public SQLManagerCustomize mySQLManagerCustomize(){
		return new SQLManagerCustomize(){
			@Override
			public void customize(String sqlMangerName, SQLManager manager) {
				//初始化sql，这里也可以对sqlManager进行修改
				manager.setNc(new UnderlinedNameConversion());
				manager.setInters(new Interceptor[0]);

			}
		};
	}
}
